﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Beeper.Engine.Test
{
    /// <summary>
    /// Ces tests permettent de vérifier la bonne implémentation de Beeper.Engine
    /// 
    /// L'objectif est de faire en sorte que tous les tests passent :
    /// sans modifier le test lui même ; sinon ça serait de la triche!
    /// </summary>
    [TestClass]
    public class UnitTest_A
    {
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Alors la date de création du service est valide et proche de la date/heure actuelle
        /// </summary>
        [TestMethod]
        public void A_Create_new_BeeperStore_Has_A_CreatedDate()
        {
            DateTime dateTimeBeforeCreationOfBeepService = DateTime.Now;

            BeepService service = new BeepService();
            //On s'assure qu'une valeur a bien été affectée à CreatedDate
            Assert.IsNotNull(service.CreatedDate);
            //On teste que l'instanciation de service ne dure pas plus d'une minute
            Assert.IsTrue((dateTimeBeforeCreationOfBeepService - service.CreatedDate).Value.TotalMinutes <= 1);
        }
        /// <summary>
         /// Lorsqu'on crée un nouveau service de gestion des Beep
         /// Et que l'on publie un nouveau beep
         /// Alors ce beep comporte un identifiant différent de 000-0000-000-000
         /// </summary>
        [TestMethod]
        public void A2_Publish_a_new_beep_and_verify_his_guid()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#beepbug", "ceci est un beep'");
            service.Push(newBeepCmd);

            Assert.IsTrue(service.GetAllBeeps().First().BeepGuid != new Guid(), "Le BeepGuid a une valeur incorecte : 000-000-000-000");
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec un message qui dépasse les 25 caractères
        /// Alors on obtient une ArgumentException
        /// </summary>
        [TestMethod]
        public void B_Publish_a_beep_with_too_long_message()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#beepbug", "ceci est un beep avec un message... qui dépasse de loin la taille maximale autorisée par notre killer app'");

            Assert.ThrowsException<ArgumentException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec un message inférieur à 3 caractères
        /// Alors on obtient une ArgumentException
        /// </summary>
        [TestMethod]
        public void C_Publish_a_beep_with_too_short_message()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#beepbug", "hi");

            Assert.ThrowsException<ArgumentException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec un hastag supérieur à 10 Caracteres
        /// Alors on obtient une ArgumentException
        /// </summary>
        [TestMethod]
        public void D_Publish_a_beep_with_too_long_hashtag()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#beepbugveryverylong", "hiveijjeziovne");

            Assert.ThrowsException<ArgumentException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec un hastag inférieur à 3 Caracteres
        /// Alors on obtient une ArgumentException
        /// </summary>
        [TestMethod]
        public void E_Publish_a_beep_with_too_short_hashtag()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#", "hiveijjeziovne");

            Assert.ThrowsException<ArgumentException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep sans préciser de tag
        /// Alors on obtient une ArgumentNullException
        /// </summary>
        [TestMethod]
        public void F_Publish_a_beep_without_hashtag()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], null, "hiveijjeziovne");

            Assert.ThrowsException<ArgumentNullException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep sans préciser de message
        /// Alors on obtient une ArgumentNullException
        /// </summary>
        [TestMethod]
        public void G_Publish_a_beep_without_message()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], "#vide", null);

            Assert.ThrowsException<ArgumentNullException>(() => service.Push(newBeepCmd));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec des valeurs correctes
        /// Alors on obtient un résultat lorsque l'on compte nos beeps
        /// Et les valeurs enregistrées sont correctes
        /// </summary>
        [TestMethod]
        public void H_Publish_a_correct_beep_and_verify_count_and_values()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);
            string tag = "#first";
            string message = "My first beep!";

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], tag, message);
            service.Push(newBeepCmd);


            Assert.IsTrue(service.GetAllBeeps().Count() == 1);
            Assert.IsTrue(service.GetAllBeeps().First().Message == message);
            Assert.IsTrue(service.GetAllBeeps().First().Hastag == tag);
            Assert.IsTrue(service.GetAllBeeps().First().UserGuid == users[0]);
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec des valeurs correctes
        /// Et qu'un utilisateur fait un vote up
        /// Alors le beep doit avoir un VoteUp à 1
        /// </summary>
        [TestMethod]
        public void I_Publish_a_correct_beep_and_a_user_vote_up()
        {
            BeepService service = GetNewBeepService_WithOneBeepPushed();
            var users =  GetNewFakeUsers(1);

            var beep = service.GetAllBeeps().First();
            service.Push(new Commands.VoteUpTheBeepCmd(users[0], beep.BeepGuid));

            Assert.IsTrue(beep.VoteUpCount == 1);
        }
         /// <summary>
         /// Lorsqu'on crée un nouveau service de gestion des Beep
         /// Et que l'on poste un beep avec des valeurs correctes
         /// Et qu'un utilisateur fait un vote up
         /// Et que ce même utilisateur renvoit un voteup
         /// Alors on obtient une ApplicationException
         /// </summary>
        [TestMethod]
        public void J_Publish_a_correct_beep_but_a_user_vote_up_two_times()
        {
            BeepService service = GetNewBeepService_WithOneBeepPushed();
            var users = GetNewFakeUsers(1);

            var beep = service.GetAllBeeps().First();
            var newVoteUp = new Commands.VoteUpTheBeepCmd(users[0], beep.BeepGuid);
            service.Push(newVoteUp);

            Assert.ThrowsException<ApplicationException>(() => service.Push(newVoteUp));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec des valeurs correctes
        /// Et qu'un utilisateur fait un votedown
        /// Et que ce même utilisateur renvoit un votedown
        /// Alors on obtient une ApplicationException
        /// </summary>
        [TestMethod]
        public void K_Publish_a_correct_beep_but_a_user_vote_down_two_times()
        {
            BeepService service = GetNewBeepService_WithOneBeepPushed();
            var users = GetNewFakeUsers(1);

            var beep = service.GetAllBeeps().First();
            var newVoteDown = new Commands.VoteDownTheBeepCmd(users[0], beep.BeepGuid);
            service.Push(newVoteDown);

            Assert.ThrowsException<ApplicationException>(() => service.Push(newVoteDown));
        }
        /// <summary>
        /// Lorsqu'on crée un nouveau service de gestion des Beep
        /// Et que l'on poste un beep avec des valeurs correctes
        /// Et qu'un utilisateur fait un vote up
        /// Et que ce même utilisateur envoit un votedown
        /// Alors on obtient un beep à 0 voteup et 1 vote down
        /// </summary>
        [TestMethod]
        public void L_Publish_a_correct_beep_and_a_user_vote_up_and_vote_down()
        {
            BeepService service = GetNewBeepService_WithOneBeepPushed();
            var users = GetNewFakeUsers(1);

            var beep = service.GetAllBeeps().First();
            var newVoteUp = new Commands.VoteUpTheBeepCmd(users[0], beep.BeepGuid);
            service.Push(newVoteUp);

            var newVoteDown = new Commands.VoteDownTheBeepCmd(users[0], beep.BeepGuid);
            service.Push(newVoteDown);

            Assert.IsTrue(beep.VoteUpCount == 0 && beep.VoteDownCount == 1);
        }

        #region SOME_STUFF_FOR_SIMPLIFY_TESTS
        public Guid[] GetNewFakeUsers(int userCountToCreate)
        {
            List<Guid> guids = new List<Guid>();
            for (int i = 0; i < userCountToCreate; i++)
                guids.Add(Guid.NewGuid());
            return guids.ToArray();
        }
        public BeepService GetNewBeepService_WithOneBeepPushed()
        {
            BeepService service = new BeepService();
            var users = GetNewFakeUsers(1);
            string tag = "#first";
            string message = "My first beep!";

            var newBeepCmd = new Commands.PublishNewBeepCmd(users[0], tag, message);
            service.Push(newBeepCmd);
            return service;
        }


    }


    #endregion
}


