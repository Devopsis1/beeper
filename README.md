# Beeper ! Le nouveau twitter en plus léger !

Beeper reprend quelques règles métier de twitter, il vous faut les implémenter.
> Pour cela, vous n'interviendrez que dans un seul fichier : Beep.cs (consultez le TODO de ce fichier!)

Bien que le périmètre fonctionnel soit assez simple, le code présenté dans cette solution est très proche de celui que l'on peut trouver dans des architectures professionnelles avancées.

# Objectifs à atteindre

## Implémenter des fonctionnalités
* Apprendre à implémenter une interface fournie
* Intégrer de nouvelles fonctionnalités en respectant l'architecture fournie

## Tester
* Apprendre à exécuter des tests unitaires fournis
* Corriger l'implémentation pour valider tous les tests
* Apprendre à créer de nouveaux tests unitaires