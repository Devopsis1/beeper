﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beeper.Engine.Commands;

namespace Beeper.Engine.Domain
{
    //TODO : Ajouter ici une classe MyBeep qui implémente l'interface IBeep
    //
    // Le programme doit alors compiler après cette implémentation
    //
    // Il faut ensuite faire évoluer MyBeep pour que les tests de UnitTest_A s'exécutent avec succès
    public interface IBeep
    {
        Guid BeepGuid { get; }
        Guid UserGuid { get; }

        /// <summary>
        /// Devra être implémentée en public
        /// Le BeepGuid devra être affecté automatiquement dans cette méthode
        /// (pour cela, faire une recherche sur le Type Guid)
        /// </summary>
        IBeep NewBeep(Commands.PublishNewBeepCmd cmd);

        void Handle(Commands.VoteUpTheBeepCmd cmd);
        void Handle(Commands.VoteDownTheBeepCmd cmd);

        void Handle(Commands.PublishCommentCmd cmd);

        //must be < 10 characters, see STRING_SIZE_LIMITS
        String Hastag { get; }
        //must be < 25 characters, see STRING_SIZE_LIMITS
        String Message { get; }

        int VoteUpCount { get; }
        int VoteDownCount { get; }
    }

    public struct STRING_SIZE_LIMITS
    {
        public const int BEEP_HASHTAG_MAX_SIZE = 10;
        public const int BEEP_MESSAGE_MAX_SIZE = 25;
        public const int BEEP_HASHTAG_MIN_SIZE = 3;
        public const int BEEP_MESSAGE_MIN_SIZE = 3;

        public const int COMMENT_MESSAGE_MAX_SIZE = 25;
        public const int COMMENT_MESSAGE_MIN_SIZE = 3;
    }
        
}
