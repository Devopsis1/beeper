﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beeper.Engine.Commands
{
    public class PublishNewBeepCmd
    {
        public PublishNewBeepCmd(Guid userGuid, string hashTag, string message)
        {
            UserGuid = userGuid;
            Message = message;
            Hashtag = hashTag;
        }

        public Guid UserGuid { get; private set; }
        public string Message { get; private set; }
        public string Hashtag { get; private set; }
    }
    
    public class VoteUpTheBeepCmd
    {
        public VoteUpTheBeepCmd(Guid userGuid, Guid beepGuid)
        {
            UserGuid = userGuid;
            BeepGuid = beepGuid;
        }

        public Guid UserGuid { get; private set; }
        public Guid BeepGuid { get; private set; }
    }

    public class VoteDownTheBeepCmd
    {
        public VoteDownTheBeepCmd(Guid userGuid, Guid beepGuid)
        {
            UserGuid = userGuid;
            BeepGuid = beepGuid;
        }

        public Guid UserGuid { get; private set; }
        public Guid BeepGuid { get; private set; }
    }

    public class PublishCommentCmd
    {
        public PublishCommentCmd(Guid beepGuid, string message)
        {
            BeepGuid = beepGuid;
            Message = message;
        }

        public Guid BeepGuid { get; private set; }
        public string Message { get; private set; }
    }
}
