﻿using Beeper.Engine.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beeper.Engine
{
    public class BeepService 
    {
        public DateTime? CreatedDate { get; private set; }
        BeepStore _beepStore;

        public BeepService() 
        {
            CreatedDate = DateTime.Now;
            _beepStore = new BeepStore();
        }

        public void Push(Commands.PublishNewBeepCmd cmd)
        {
            IBeep newBeep = null;
            newBeep = new MyBeep().NewBeep(cmd);
            _beepStore.Insert(newBeep);

        }
        public void Push(Commands.VoteUpTheBeepCmd cmd)
        {
            IBeep newBeep = _beepStore.GetBeepById(cmd.BeepGuid);
            newBeep.Handle(cmd);
        }
        public void Push(Commands.VoteDownTheBeepCmd cmd)
        {
            IBeep newBeep = _beepStore.GetBeepById(cmd.BeepGuid);
            newBeep.Handle(cmd);
        }
        public void Push(Commands.PublishCommentCmd cmd)
        {
            IBeep newBeep = _beepStore.GetBeepById(cmd.BeepGuid);
            newBeep.Handle(cmd);
        }

        public IEnumerable<IBeep> GetAllBeeps()
        {
            return _beepStore.GetAll();
        }

    }

    /// <summary>
    /// Store the beeps
    /// </summary>
    internal class BeepStore
    {
        private List<IBeep> _beeps;
        
        public BeepStore()
        {
            _beeps = new List<IBeep>();
        }

        public IBeep GetBeepById(Guid guid)
        {
            var beeps = _beeps.Where(x => x.BeepGuid == guid);
            if (beeps.Count() == 0)
            {
                throw new ApplicationException("beep not exists : " + guid.ToString());
            }
            return beeps.First();

        }

        public void Insert(IBeep beep)
        {
            if (_beeps.Where(x=>x.BeepGuid == beep.BeepGuid).Count() >0)
            {
                throw new ApplicationException("This beep already exists : " + beep.BeepGuid.ToString());
            }
            _beeps.Add(beep);
        }

        public IEnumerable<IBeep> GetAll()
        {
            return _beeps;
        }

    }
}
